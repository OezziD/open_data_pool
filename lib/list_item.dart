import 'package:flutter/material.dart';
import 'package:open_data_pool/detail_view.dart';
import 'package:open_data_pool/http_authorization.dart';
import 'package:open_data_pool/request_search_free_text.dart';
import 'package:open_data_pool/requests.dart';
import 'package:swagger/api.dart';

typedef SearchFunction = Function({String searchText, int page, bool append});

class ListItem {
  Product item;
  Widget widget;
  bool freeTextSearchItem = false;
  static bool isLoading = false;
  void Function({bool gotoDetailsPage, GTIN gtin}) doSearchGTIN;

  Widget generateWidget(BuildContext context, {bool bookmarked = false}) {
    widget = GestureDetector(
      // key: Key(DateTime.now().millisecondsSinceEpoch.toString()),
      key: Key(item.gtin.toString()),
      onTapDown: (tapDownDetails) {
        if (this.freeTextSearchItem) {
          doSearchGTIN(gotoDetailsPage: true, gtin: item.gtin);
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return DetailView(
                title: item.basic?.productShortDescr?.toString() ?? "",
                product: item,
              );
            }),
          );
        }
      },
      child: Card(
        shadowColor: Colors.black45,
        color: bookmarked ? Colors.green : Colors.white54,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
        margin: EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 10,
        ),
        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: 0,
            vertical: 10,
          ),
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.symmetric(
                  vertical: 0,
                  horizontal: 10,
                ),
                child: FutureBuilder(
                  future: Requests.getImage(
                      item.pictures?.pictures?.first?.urlThumbnail, MyHttpAuthorization.getCurrentToken(),
                      width: 60),
                  builder: (context, AsyncSnapshot<Image> snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data;
                    } else {
                      return Icon(
                        Icons.file_download,
                        size: 60,
                      );
                    }
                  },
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    RichText(
                      overflow: TextOverflow.clip,
                      maxLines: 1,
                      text: TextSpan(
                        text: item.basic.productShortDescr,
                        style: TextStyle(
                          color: bookmarked ? Colors.white : Colors.deepPurple,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    RichText(
                      overflow: TextOverflow.fade,
                      maxLines: 2,
                      text: TextSpan(
                        text: item.descriptions.shorttext1,
                        style: TextStyle(
                          color: bookmarked ? Colors.white : Colors.deepPurple,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );

    return widget;
  }

  Widget generateListItemFromProduct(
    Product item,
    BuildContext context, {
    bool bookmarked: false,
    bool freeTextSearchItem = false,
    void Function({bool gotoDetailsPage, GTIN gtin}) func,
  }) {
    this.item = item;
    this.freeTextSearchItem = freeTextSearchItem;
    this.doSearchGTIN = func;
    return this.generateWidget(context, bookmarked: bookmarked);
  }

  Future<List<Widget>> generateListItemsFromProductList(List<Product> products, BuildContext context) async {
    List<Widget> itemsList = [];
    debugPrint("generateListItemsFromProductList: products input count ${products.length}");
    products?.forEach((product) async {
      itemsList.add(ListItem().generateListItemFromProduct(product, context, bookmarked: true));
    });
    debugPrint("generateListItemsFromProductList: itemsList count: ${itemsList.length}");
    return itemsList;
  }

  static ElevatedButton returnMoreButton(
    FreeTextResponse freeTextResponse,
    int currentShownHitsCount,
    SearchFunction searchFunction,
    Function() setState,
  ) {
    if (freeTextResponse.hitCount > currentShownHitsCount) {
      String buttonText = "Lade ${currentShownHitsCount + 1} - ${currentShownHitsCount + freeTextResponse.pageSize}";
      return ElevatedButton(
          onPressed: () {
            isLoading = true;
            setState();
            searchFunction(searchText: freeTextResponse.searchText, page: freeTextResponse.page + 1, append: true);
          },
          child: isLoading
              ? Text("Lade...")
              : Text(buttonText));
    }

    return null;
  }
}
