import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as ImageLib;
import 'package:open_data_pool/http_authorization.dart';
import 'package:open_data_pool/requests.dart';
import 'package:photo_view/photo_view.dart';
import 'package:swagger/api.dart';

class ImageView extends StatefulWidget {
  Picture pic;

  ImageView(this.pic);

  @override
  State<StatefulWidget> createState() => _ImageViewState(pic);
}

class _ImageViewState extends State<ImageView> {
  Picture pic;

  MemoryImage img = null;

  @override
  _ImageViewState(this.pic) {
    Requests.getRawData(pic.url, MyHttpAuthorization.getCurrentToken()).then((value) {
      String lowerCaseFileName = pic.filename.toLowerCase();
      if (lowerCaseFileName.endsWith("tif") || lowerCaseFileName.endsWith("tiff")) {
        img = MemoryImage(ImageLib.encodeJpg(ImageLib.decodeTiff(value)));
      } else {
        img = MemoryImage(value);
      }
      setState(() {});
    }, onError: (error) {
      debugPrint(error);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.pic.filename),
        centerTitle: true,
      ),
      body: Container(
        child: img == null
            ? Text("Downloading....", style: TextStyle(color: Colors.white))
            : PhotoView(
                imageProvider: img,
              ),
      ),
      backgroundColor: Colors.black,
    );
  }
}
