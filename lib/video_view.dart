import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:open_data_pool/http_authorization.dart';
import 'package:open_data_pool/requests.dart';
import 'package:path_provider/path_provider.dart';
import 'package:swagger/api.dart';
import 'package:video_player/video_player.dart';

class VideoView extends StatefulWidget {
  Document doc;

  VideoView(this.doc);

  @override
  _VideoViewState createState() => _VideoViewState(this.doc);
}

class _VideoViewState extends State<VideoView> {
  VideoPlayerController _controller = null;
  double currentDownloadValue = 0;
  Document doc;
  Directory downloadDirectory = null;
  bool fileIsDownloaded = false;

  _VideoViewState(this.doc);

  void progressCallback(int count, int total) {
    setState(() {
      currentDownloadValue = count / total;
    });
  }

  void finalCallback(bool error) {
    debugPrint("Final callback: $error");
    this.fileIsDownloaded = !error;
    File f = File(downloadDirectory.absolute.path + "/" + doc.filename);

    _controller = VideoPlayerController.file(f);
    _controller.addListener(() {
      setState(() {});
    });

    _controller.setLooping(false);
    _controller.initialize().then((_) => setState(() {}));
    _controller.play();
  }

  void initState() {
    super.initState();

    getExternalStorageDirectory().then((value) {
      downloadDirectory = value;
      File f = File(downloadDirectory.absolute.path + "/" + doc.filename);
      if (f.existsSync()) {
        fileIsDownloaded = true;
        _controller = VideoPlayerController.file(f);
        _controller.addListener(() {
          setState(() {});
        });

        _controller.setLooping(false);
        _controller.initialize().then((_) => setState(() {}));
        _controller.play();
      } else {
        Requests.saveFile(
            doc.url,
            downloadDirectory.absolute.path + "/" + doc.filename,
            MyHttpAuthorization.getCurrentToken(),
            progressCallback,
            finalCallback);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: fileIsDownloaded
            ? AspectRatio(
                aspectRatio: 1.5,
                child: VideoPlayer(_controller),
              )
            : LinearProgressIndicator(
                value: currentDownloadValue,
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (fileIsDownloaded) {
            setState(() {
              _controller.value.isPlaying
                  ? _controller.pause()
                  : _controller.play();
            });
          }
          ;
        },
        child: Icon(
          fileIsDownloaded
              ? _controller.value.isPlaying
                  ? Icons.pause
                  : Icons.play_arrow
              : Icons.help_outline,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.pause();
    _controller.dispose();
  }
}
