import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:open_data_pool/http_authorization.dart';
import 'package:open_data_pool/image_view.dart';
import 'package:open_data_pool/pdf_view.dart';
import 'package:open_data_pool/requests.dart';
import 'package:open_data_pool/storage.dart';
import 'package:open_data_pool/video_view.dart';
import 'package:swagger/api.dart';

class DetailView extends StatefulWidget {
  DetailView({Key key, this.title, this.product}) : super(key: key);

  String title;
  Product product;

  @override
  State<StatefulWidget> createState() => _DetailViewState(title, product);
}

class _DetailViewState extends State<DetailView> {
  String title = "";
  Product product = null;
  bool isBookmarked = false;

  _DetailViewState(this.title, this.product) {
    isBookmarked = this.product.isBookmarked;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.title),
        centerTitle: true,
        leading: BackButton(),
      ),
      body: ListView(
        shrinkWrap: true,
        children: getWidgetsForProduct(),
      ),
      floatingActionButton: FloatingActionButton(
        child: isBookmarked ? Icon(Icons.favorite) : Icon(Icons.favorite_border_outlined),
        onPressed: () {
          if (this.isBookmarked) {
            product.isBookmarked = false;
            this.isBookmarked = false;
            Storage.deleteProductLocally(product);
          } else {
            Storage.saveProductLocally(product);
            product.isBookmarked = true;
            this.isBookmarked = true;
          }
          setState(() {});
        },
      ),
    );
  }

  List<ListTile> getWidgetsForProduct() {
    if (this.product == null) return [];
    List<ListTile> entries = [];

    generateListTileFromGivenData(product.basic.productShortDescr, entries);
    generateListTileFromGivenData(product.basic.commodityGroupDescr, entries);
    // generateListTileFromGivenData(product.basic.commodityGroupId, entries);
    generateListTileFromGivenData(product.basic.mainCommodityGroupDescr, entries);
    // generateListTileFromGivenData(product.basic.matchcode, entries);
    // generateListTileFromGivenData(product.basic.modelNumber, entries);
    // generateListTileFromGivenData(product.basic.noteOfUse, entries);

    // generateListTileFromGivenData(product.descriptions.shorttext1, entries);
    generateListTileFromGivenData(product.descriptions.marketingText, entries);
    generateListTileFromGivenData(product.descriptions.productDescr, entries);
    // generateListTileFromGivenData(product.descriptions.shorttext2, entries);

    product.documents.listDocuments.forEach((element) {
      generateListTileFromGivenData(element, entries);
    });

    product.pictures.pictures.forEach((element) {
      generateListTileFromGivenData(element, entries);
    });

    // product.sparepartlists.sparepartlistRow.forEach((element) {
    //   generateListTileFromGivenData(element, entries);
    // });

    // generateListTileFromGivenData(product.logistics.exportable, entries);
    // generateListTileFromGivenData(product.logistics.commodityNumber, entries);
    // generateListTileFromGivenData(product.logistics.countryOfOrigin, entries);
    // generateListTileFromGivenData(product.logistics.hazardousMaterial, entries);
    // generateListTileFromGivenData(product.logistics.unNumber, entries);
    // generateListTileFromGivenData(product.logistics.dangerClass, entries);
    // generateListTileFromGivenData(product.logistics.carryingCategory, entries);
    // generateListTileFromGivenData(product.logistics.reachInfo, entries);
    // generateListTileFromGivenData(product.logistics.reachDate, entries);
    // generateListTileFromGivenData(product.logistics.ubaListRelevant, entries);
    // generateListTileFromGivenData(product.logistics.ubaListConform, entries);
    // generateListTileFromGivenData(product.logistics.durabilityPeriod, entries);
    // generateListTileFromGivenData(
    //     product.logistics.standardDeliveryPeriod, entries);
    // generateListTileFromGivenData(product.logistics.lucidNumber, entries);
    // generateListTileFromGivenData(
    //     product.logistics.packagingDisposalProvider, entries);
    // generateListTileFromGivenData(product.logistics.weeeNumber, entries);
    // generateListTileFromGivenData(product.logistics.measureA, entries);
    // generateListTileFromGivenData(product.logistics.measureB, entries);
    // generateListTileFromGivenData(product.logistics.measureC, entries);
    // generateListTileFromGivenData(product.logistics.weight, entries);
    // generateListTileFromGivenData(product.logistics.packagingQuantity, entries);
    // generateListTileFromGivenData(product.logistics.packagingUnits, entries);

    return entries;
  }

  void generateListTileFromGivenData(dynamic data, List list) {
    if (data != null) {
      switch (data.runtimeType) {
        case Picture:
          list.add(createPictureListTile(data as Picture));
          break;
        case Document:
          Widget w = createDocumentListTile(data as Document);
          if (w != null) {
            list.add(w);
          }
          break;
        case Measure:
          Measure m = data as Measure;
          if (m.measure != null) {
            list.add(createMeasureListTile(data as Measure));
          }
          break;
        case Weight:
          Weight w = data as Weight;
          if (w.weight != null) {
            list.add(createWeightListTile(data as Weight));
          }
          break;
        default:
          if (data.toString().length > 0) {
            list.add(
              ListTile(
                title: Container(child: Text(parseString(data.toString()))),
              ),
            );
          }
      }
    }
  }

  String parseString(String text) {
    String newText = text;
    return newText
        .replaceAll("&gt;", ">")
        .replaceAll("&lt;", "<")
        .replaceAll("&br;", "\n")
        .replaceAll("<br />","\n")
        .replaceAll("<br>", "\n")
        .replaceAll("&amp;", "&");
  }

  ListTile createPictureListTile(Picture data) {
    return ListTile(
      title: Text(data.filename?.toString() ?? ""),
      subtitle: Text(data.description?.toString() ?? ""),
      leading: FutureBuilder<Image>(
        future: Requests.getImage(data.urlThumbnail, MyHttpAuthorization.getCurrentToken(), width: 80),
        builder: (context, AsyncSnapshot<Image> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data == null) {
              return Icon(Icons.error_outline);
            } else {
              return snapshot.data;
            }
          } else {
            return Icon(
              Icons.file_download,
              size: 80,
            );
          }
        },
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ImageView(data)),
        );
      },
    );
  }

  ListTile createDocumentListTile(Document data) {
    bool isPDF = data.filename.toLowerCase().endsWith("pdf");
    bool isMovie = false;

    var dataTypeValue = data.type.value;
    var icon = isPDF ? Icons.picture_as_pdf : Icons.insert_drive_file;

    if (dataTypeValue == DocumentType.vI_.value) {
      icon = Icons.ondemand_video_rounded;
      isMovie = true;
    } else if (dataTypeValue == DocumentType.pF_.value ||
        dataTypeValue == DocumentType.eL_.value ||
        dataTypeValue == DocumentType.dB_.value ||
        dataTypeValue == DocumentType.iS_.value ||
        dataTypeValue == DocumentType.pA_.value ||
        dataTypeValue == DocumentType.wA_.value ||
        dataTypeValue == DocumentType.lE_.value ||
        dataTypeValue == DocumentType.tZ_.value) {
      // dataTypeValue == DocumentType.THREEC_.value ||
      //     dataTypeValue == DocumentType.TWOD_.value) {
      icon = Icons.picture_as_pdf;
    } else if (dataTypeValue == DocumentType.mA_.value) {
      icon = Icons.build_circle_outlined;
    } else {
      debugPrint("unknown file format: $dataTypeValue");
      return null;
    }

    return ListTile(
      title: Text(data.filename?.toString() ?? ""),
      subtitle: Text(data.description?.toString() ?? ""),
      leading: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
        child: Icon(
          icon,
          color: Colors.red,
          size: 60,
        ),
      ),
      onTap: () {
        if (isPDF) {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => PDFViewer(data)),
          );
        } else if (isMovie) {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => VideoView(data)),
          );
        }
      },
    );
  }

  ListTile createMeasureListTile(Measure data) {
    return ListTile(
      title: Text((data.measure?.toString() ?? "") + " " + (data.unit?.toString() ?? "")),
    );
  }

  ListTile createWeightListTile(Weight data) {
    return ListTile(
      title: Text((data.weight?.toString() ?? "") + " " + (data.unit?.toString() ?? "")),
    );
  }
}
