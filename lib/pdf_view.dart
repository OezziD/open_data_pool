import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:native_pdf_view/native_pdf_view.dart';
import 'package:open_data_pool/http_authorization.dart';
import 'package:open_data_pool/requests.dart';
import 'package:path_provider/path_provider.dart';
import 'package:swagger/api.dart';

class PDFViewer extends StatefulWidget {
  Document doc;

  PDFViewer(this.doc);

  @override
  State<StatefulWidget> createState() => _PDFViewerState(doc);
}

class _PDFViewerState extends State<PDFViewer> {
  Document doc;
  PdfController _pdfController;
  Directory downloadDirectory;
  bool fileIsDownloaded = false;
  double currentDownloadValue = 0;

  _PDFViewerState(this.doc);

  void progressCallback(int count, int total) {
    this.setState(() {
      this.currentDownloadValue = count / total;
    });
  }

  void finalCallback(bool error) {
    if (!error) {
      this.fileIsDownloaded = true;
      _pdfController =
          PdfController(document: PdfDocument.openFile(downloadDirectory.absolute.path + "/" + doc.filename));
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    getExternalStorageDirectory().then((value) {
      downloadDirectory = value;
      File f = File(downloadDirectory.absolute.path + "/" + doc.filename);
      if (f.existsSync()) {
        fileIsDownloaded = true;
        this._pdfController = PdfController(document: PdfDocument.openFile(f.path));
        this.setState(() {});
      } else {
        Requests.saveFile(
          doc.url,
          f.absolute.path,
          MyHttpAuthorization.getCurrentToken(),
          progressCallback,
          finalCallback,
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(this.doc.filename),
          centerTitle: true,
        ),
        body: this.fileIsDownloaded
            ? PdfView(
                controller: this._pdfController,
                scrollDirection: Axis.vertical,
              )
            : LinearProgressIndicator(
                value: currentDownloadValue,
              ));
  }
}
