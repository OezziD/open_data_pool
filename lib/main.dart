import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:open_data_pool/detail_view.dart';
import 'package:open_data_pool/http_authorization.dart';
import 'package:open_data_pool/http_authorization_itek.dart';
import 'package:open_data_pool/list_item.dart';
import 'package:open_data_pool/request_search_free_text.dart';
import 'package:open_data_pool/requests.dart';
import 'package:open_data_pool/search_types.dart';
import 'package:open_data_pool/storage.dart';
import 'package:open_data_pool/version.dart';
import 'package:package_info/package_info.dart';
import 'package:swagger/api.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

void fetchToken() async {
  await MyHttpAuthorization.access_token;
}

class MyApp extends StatelessWidget {
  @override
  MyApp() : super() {
    fetchToken();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Open Data Pool',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Open Data Pool'),
      builder: EasyLoading.init(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<List<Widget>> listItems = [[], [], [], []];
  List<Widget> bookmarkedItems = [];

  //treffer die aus der suche stammen
  List<List<Product>> resultItems = [[], [], [], []];

  TextEditingController searchControllerGTIN = TextEditingController(text: "4050663093402");
  TextEditingController searchControllerManID = TextEditingController(text: "oventrop");
  TextEditingController searchControllerManPid = TextEditingController(text: "1012166");
  TextEditingController searchControllerManIDType = TextEditingController(text: "opendatapool");
  TextEditingController searchControllerSupPid = TextEditingController(text: "");

  TextEditingController searchControllerFreeText = TextEditingController(text: "");

  SearchTypes dropDownValue = SearchTypes.GTIN;
  int lastTimeUpdated = 0;
  bool fetching = false;
  bool drawerChanged = false;

  PackageInfo pInfo;
  String appInfo = "";

  FreeTextResponse freeTextResponse = FreeTextResponse([], 0, 0, 0, "");

  TextEditingController loginController = TextEditingController(text: "");
  TextEditingController passwordController = TextEditingController(text: "");

  _MyHomePageState() {
    MyHttpAuthorization.refreshToken();
    Storage.addListener(this.setState);
  }

  @override
  void initState() {
    super.initState();
    PackageInfo.fromPlatform().then((value) {
      pInfo = value;
      setAppInfo();
    });
  }

  fetchSavedProducts() {
    this.fetching = true;
    Storage.getAllSavedProducts().then((products) {
      ListItem().generateListItemsFromProductList(products, context).then((widgetList) {
        this.setState(() {
          this.bookmarkedItems = widgetList;
          this.lastTimeUpdated = DateTime.now().millisecondsSinceEpoch;
          this.fetching = false;
        });
      });
    });
  }

  Widget buildDropDown() {
    return Row(children: [
      Expanded(
        child: Center(
          child: DropdownButton(
            items: [
              DropdownMenuItem(
                value: SearchTypes.GTIN,
                child: Text("Suche über GTIN"),
              ),
              DropdownMenuItem(
                value: SearchTypes.MANUFACTURER,
                child: Text("Suche über Hersteller"),
              ),
              DropdownMenuItem(
                value: SearchTypes.SUPPLIER,
                child: Text("Suche über Anbieter"),
              ),
              DropdownMenuItem(
                value: SearchTypes.FREE_TEXT,
                child: Text("Freitextsuche"),
              ),
            ],
            value: dropDownValue,
            onChanged: (var newValue) {
              setState(() {
                dropDownValue = newValue;
              });
            },
          ),
        ),
      ),
    ]);
  }

  Widget buildSearchGTIN() {
    return Row(
      children: [
        Expanded(
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            child: TextField(
              textAlign: TextAlign.center,
              controller: searchControllerGTIN,
              keyboardType: TextInputType.number,
              style: TextStyle(),
              decoration: InputDecoration(labelText: "GTIN"),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          child: ElevatedButton(
            onPressed: doSearchGTIN,
            child: Text('Suche'),
          ),
        ),
      ],
    );
  }

  Widget buildSearchManufacturer() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                child: TextField(
                  textAlign: TextAlign.center,
                  controller: searchControllerManID,
                  style: TextStyle(),
                  decoration: InputDecoration(labelText: "Hersteller ID"),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                child: TextField(
                  textAlign: TextAlign.center,
                  controller: searchControllerManPid,
                  style: TextStyle(),
                  decoration: InputDecoration(labelText: "Hersteller Produkt ID"),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                child: TextField(
                  textAlign: TextAlign.center,
                  controller: searchControllerManIDType,
                  style: TextStyle(),
                  decoration: InputDecoration(labelText: "Hersteller ID Typ"),
                ),
              ),
            ),
          ],
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          child: ElevatedButton(
            onPressed: doSearchManufacturer,
            child: Text('SEARCH'),
          ),
        ),
      ],
    );
  }

  Widget buildSearchSupplier() {
    return Row(
      children: [
        Expanded(
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            child: TextField(
              textAlign: TextAlign.center,
              controller: searchControllerSupPid,
              keyboardType: TextInputType.number,
              style: TextStyle(),
              decoration: InputDecoration(labelText: "Lieferanten Produkt ID"),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          child: ElevatedButton(
            onPressed: doSearchSupplier,
            child: Text('Suche'),
          ),
        ),
      ],
    );
  }

  Widget buildSearchFreeText() {
    return Row(
      children: [
        Expanded(
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            child: TextField(
              textAlign: TextAlign.center,
              controller: searchControllerFreeText,
              keyboardType: TextInputType.text,
              style: TextStyle(),
              decoration: InputDecoration(labelText: "Freitext"),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          child: ElevatedButton(
            onPressed: doSearchFreeText,
            child: Text('Suche'),
          ),
        ),
      ],
    );
  }

  Widget getSearchMaskByDropDownValue(SearchTypes dropDownValue) {
    switch (dropDownValue) {
      case SearchTypes.GTIN:
        return buildSearchGTIN();
      case SearchTypes.MANUFACTURER:
        return buildSearchManufacturer();
      case SearchTypes.SUPPLIER:
        return buildSearchSupplier();
      case SearchTypes.FREE_TEXT:
        return buildSearchFreeText();
      default:
        throw new Exception("Unknown DropDownValue : $dropDownValue");
    }
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("lastTimeUpdated: ${this.lastTimeUpdated.toString()}");
    debugPrint("Storage last Time changed: ${Storage.getLastTimeChanged().toString()}");

    if (this.lastTimeUpdated == 0 || Storage.getLastTimeChanged() > this.lastTimeUpdated) {
      debugPrint("needs refresh of bookmarkedlist");
      debugPrint("is currently fetching: ${this.fetching}");
      if (!this.fetching) {
        this.fetchSavedProducts();
      }
    }
    return Scaffold(
      appBar: AppBar(
        title: Column(
          children: [
            // Row(
            //   children: [
            //     Expanded(
            //       child: Center(
            //         child: Text("Open Data Pool"),
            //       ),
            //     ),
            //   ],
            // ),
            buildDropDown(),
          ],
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          getSearchMaskByDropDownValue(dropDownValue),
          dropDownValue.index == SearchTypes.FREE_TEXT.index
              ? getHits()
              : Container(
                  width: 0,
                  height: 0,
                ),
          Expanded(
            child: ListView(
              children: this.listItems[dropDownValue.index] + this.bookmarkedItems,
              semanticChildCount: this.listItems[dropDownValue.index].length + this.bookmarkedItems.length,
            ),
          )
        ],
      ),
      onDrawerChanged: (bool opened) {
        if (opened && drawerChanged != opened) {
          readLoginData();
        }
        drawerChanged = opened;
      },
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              duration: Duration(milliseconds: 100),
              child: Center(
                child: Text(
                  "Login to Open Data Pool",
                ),
              ),
            ),
            ListTile(
              title: Text("Login Name:"),
              subtitle: TextField(
                textAlign: TextAlign.center,
                controller: loginController,
              ),
            ),
            Container(
              padding: EdgeInsets.all(15),
            ),
            ListTile(
              title: Text("Password:"),
              subtitle: TextField(
                textAlign: TextAlign.center,
                controller: passwordController,
                obscureText: true,
              ),
            ),
            ListTile(
              title: ElevatedButton(
                onPressed: saveLoginData,
                child: Text("Login"),
              ),
            ),
            ListTile(
              title: Text(Version.version),
              subtitle: Text(appInfo),
            ),
          ],
        ),
      ),
    );
  }

  Widget getHits() {
    return Text("${this.resultItems[SearchTypes.FREE_TEXT.index].length} / ${this.freeTextResponse.hitCount}");
  }

  void setAppInfo() async {
    pInfo = await PackageInfo.fromPlatform();
    setState(() {
      appInfo = "${pInfo.version}";
    });
  }

  void readLoginData() async {
    FlutterSecureStorage secureStorage = FlutterSecureStorage();
    Map<String, String> credentials = await secureStorage.readAll();
    loginController.text = credentials['name'];
    passwordController.text = credentials['password'];
    this.setState(() {});
  }

  void saveLoginData() {
    FlutterSecureStorage secureStorage = FlutterSecureStorage();
    secureStorage.write(key: "name", value: loginController.text);
    secureStorage.write(key: "password", value: passwordController.text);
  }

  List<String> getDataPackage() {
    return ['basic|additional|prices|descriptions|logistics|sparepartlists|pictures|documents'];
  }

  void parseResult(Product product, SearchTypes searchType) async {
    this.resultItems[searchType.index] = [product];
    this.listItems[searchType.index] = [await ListItem().generateListItemFromProduct(product, this.context)];
    this.setState(() {});
  }

  void parseResultFreeTextSearch(FreeTextResponse response, SearchTypes searchType, {bool append = false}) async {
    if (!append) {
      this.resultItems[searchType.index] = response.hits;
      this.listItems[searchType.index] = [];
    } else {
      this.resultItems[searchType.index].addAll(response.hits);
      this.listItems[searchType.index].removeLast();
    }
    response.hits.forEach((element) {
      this.listItems[searchType.index].add(ListItem().generateListItemFromProduct(
            element,
            this.context,
            freeTextSearchItem: true,
            func: this.doSearchGTIN,
          ));
    });

    Widget w = ListItem.returnMoreButton(
      freeTextResponse,
      this.listItems[searchType.index].length,
      this.doSearchFreeText,
      (() {
        this.setState(() {});
      }),
    );
    if (w != null) {
      this.listItems[searchType.index].add(w);
    }
    this.setState(() {});
  }

  void startLoading() {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }

    EasyLoading.show(status: "loading....");
  }

  void endLoading() {
    EasyLoading.dismiss();
  }

  void doSearchManufacturer() async {
    try {
      startLoading();
      String accessToken = await MyHttpAuthorization.access_token;

      var result = await Requests.getProductByManufacturerData(
        ManufacturerID(this.searchControllerManID.text),
        ManufacturerPID(this.searchControllerManPid.text),
        getDataPackage(),
        ManufacturerIDType(this.searchControllerManIDType.text),
        accessToken,
      );

      parseResult(result, SearchTypes.MANUFACTURER);
    } catch (e) {
      debugPrint(e.toString());
    } finally {
      endLoading();
    }
  }

  void doSearchGTIN({bool gotoDetailsPage = false, GTIN gtin}) async {
    try {
      startLoading();
      String accessToken = await MyHttpAuthorization.access_token;
      var result;
      // result = await api_instance.getProductByGTIN(gtin, data_package);
      //springe direkt zu der DetailSeite
      if (gotoDetailsPage) {
        result = await Requests.getProductByGTIN(
          gtin,
          getDataPackage(),
          accessToken,
        );
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return DetailView(
              title: result.basic?.productShortDescr?.toString() ?? "",
              product: result,
            );
          }),
        );
      } else {
        result = await Requests.getProductByGTIN(
          GTIN(searchControllerGTIN.text.trim()),
          getDataPackage(),
          accessToken,
        );
        parseResult(result, SearchTypes.GTIN);
      }
    } catch (e) {
      debugPrint(e.toString());
    } finally {
      endLoading();
    }
  }

  void doSearchSupplier() async {
    try {
      startLoading();
      String accessToken = await MyHttpAuthorization.access_token;
      var result;
      // result = await api_instance.getProductByGTIN(gtin, data_package);
      result = await Requests.getProductBySupplierPID(
        SupplierPID(searchControllerSupPid.text),
        getDataPackage(),
        accessToken,
      );
      //wenn die Ergebnisse zurückkommen müssen diese einzeln gesucht werden
      // bei open data pool oder ich zeige direkt die daten von hier an
      parseResult(result, SearchTypes.SUPPLIER);
    } catch (e) {
      debugPrint(e.toString());
    } finally {
      endLoading();
    }
  }

  void doSearchFreeText({String searchText = "", int page = 1, bool append = false}) async {
    try {
      //todo get the right token
      startLoading();
      String accessToken = await MyHttpAuthorizationItek.access_token;

      FreeTextResponse result = await RequestSearchFreeText.getResultList(
        searchText.length == 0 ? searchControllerFreeText.text : searchText,
        accessToken,
        page,
      );
      this.freeTextResponse = result;
      ListItem.isLoading = false;
      parseResultFreeTextSearch(result, SearchTypes.FREE_TEXT, append: append);
    } catch (e) {
      debugPrint(e.toString());
    } finally {
      endLoading();
    }
  }
}
