import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart' as PathProvider;
import 'package:swagger/api.dart';

typedef callback(VoidCallback fn);

class Storage {
  static List<callback> listRefresh = [];
  static int _lastTimeChanged = 0;

  static int getLastTimeChanged() {
    return _lastTimeChanged;
  }

  static final PRODUCT_FILE_NAME = "product.json";

  static Future<Directory> getProductDirectory(String gtin) async {
    Directory docDir = await PathProvider.getApplicationDocumentsDirectory();
    String directoryPath = docDir.path + Platform.pathSeparator + gtin;
    return Directory(directoryPath);
  }

  static addListener(callback function) {
    listRefresh.add(function);
  }

  static changeTime() {
    _lastTimeChanged = DateTime.now().millisecondsSinceEpoch;
    listRefresh.forEach((callback func) {
      func(() {});
    });
  }

  static saveProductLocally(Product product) async {
    Directory productDirectory = await getProductDirectory(product.gtin.value);
    if (!productDirectory.existsSync()) {
      productDirectory.create(recursive: true);
    }
    File productFile = File(productDirectory.path + Platform.pathSeparator + PRODUCT_FILE_NAME);
    if (!productFile.existsSync()) {
      productFile.createSync(recursive: true);
    }
    productFile.writeAsStringSync(
      product.raw,
      flush: true,
    );
    changeTime();
  }

  static deleteProductLocally(Product product) async {
    Directory productDirectory = await getProductDirectory(product.gtin.value);

    if (productDirectory.existsSync()) {
      File productFile = File(productDirectory.path + Platform.pathSeparator + PRODUCT_FILE_NAME);
      if (productFile.existsSync()) {
        productFile.deleteSync();
      }
      productDirectory.deleteSync();
      changeTime();
    }
  }

  static Future<Product> loadProduct(String gtin) async {
    Directory productDirectory = await getProductDirectory(gtin);

    if (!productDirectory.existsSync()) {
      return null;
    }

    File productFile = File(productDirectory.path + Platform.pathSeparator + gtin + PRODUCT_FILE_NAME);

    if (!productFile.existsSync()) {
      return null;
    }

    String content = productFile.readAsStringSync();
    ApiClient apiClient = ApiClient();
    Product product = apiClient.deserialize(content, 'Product') as Product;

    product.isBookmarked = true;

    return product;
  }

  static Future<List<Product>> getAllSavedProducts() async {
    List<Product> products = [];

    Directory docDir = await PathProvider.getApplicationDocumentsDirectory();
    List<FileSystemEntity> filesInDir = docDir.listSync();

    filesInDir.forEach((element) {
      if (element.statSync().type == FileSystemEntityType.directory) {
        File productFile = File(element.path + Platform.pathSeparator + PRODUCT_FILE_NAME);
        if (productFile.existsSync()) {
          String content = productFile.readAsStringSync();
          ApiClient apiClient = ApiClient();
          Product product = apiClient.deserialize(content, 'Product') as Product;
          product.isBookmarked = true;
          products.add(product);
        }
      }
    });
    return products;
  }
}
