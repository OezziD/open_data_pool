import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:open_data_pool/main.dart';
import 'package:page_transition/page_transition.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(SplashScreen());
}

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Open Data Pool',
      home: AnimatedSplashScreen(
        animationDuration: Duration(milliseconds: 1000),
        duration: 3000,
        splash: Image(
          image: AssetImage('assets/images/splash_data_pool2.png'),
        ),
        splashIconSize: 400,
        splashTransition: SplashTransition.scaleTransition,
        pageTransitionType: PageTransitionType.leftToRightWithFade,
        backgroundColor: Colors.white,
        nextScreen: MyApp(),
      ),
    );
  }
}
