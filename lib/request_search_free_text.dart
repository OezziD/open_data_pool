import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:open_data_pool/http_authorization.dart';
import 'package:open_data_pool/http_authorization_itek.dart';
import 'package:swagger/api.dart';

typedef finalCallback = void Function(bool error);

class FreeTextResponse {
  List<Product> hits;
  int pageSize = 10;
  int page = 1;
  int hitCount = 0;
  String searchText = "";

  FreeTextResponse(this.hits, this.page, this.pageSize, this.hitCount, this.searchText);
}

class RequestSearchFreeText {
  static int PAGE_SIZE = 50;

  // static Future<List<Product>> getResultList(
  static Future<FreeTextResponse> getResultList(
    String searchWords,
    String authorizationToken,
    int page, {
    int retries = 0,
  }) async {
    Dio dio = Dio();

    Options opt = Options();
    opt
      ..headers = {
        "Authorization": "Bearer $authorizationToken",
        "Content-Type": "application/json",
      }
      ..responseType = ResponseType.plain;

    var response = await dio.post(
      "https://zvshk.itek.de/api/deepsearch/v1.1.0/article/search",
      data: '{"searchTerm":"$searchWords","pageSize":${PAGE_SIZE},"page":${page},"filter":[]}',
      options: opt,
    );

    if (response == null) {
      if (retries == 0) {
        MyHttpAuthorizationItek.refreshToken();
        debugPrint("Trying to refresh the token from ITEK");
        return getResultList(searchWords, await MyHttpAuthorization.access_token, page, retries: 1);
      }
    }

    ApiClient apiClient = ApiClient();
    dynamic responseJson = apiClient.deserialize(response.data.toString(), 'Json');

    int hitCount = responseJson["hitCount"];
    int currentPage = responseJson["page"];
    int pageSize = responseJson["pageSize"];
    dynamic hits = responseJson['hits'];

    List<Product> products = [];

    if (hits != null && hits.length > 0) {
      for (var i = 0; i < hits.length; i++) {
        products.add(createProductFromHits(hits[i]['hitRows']));
      }
    }
    return FreeTextResponse(products, page, pageSize, hitCount, searchWords);
  }

  static Product createProductFromHits(dynamic hitRow) {
    Product p = Product();

    for (int i = 0; i < hitRow.length; ++i) {
      String name = hitRow[i]['columnName'];
      String value = hitRow[i]['columnValue'];
      switch (name) {
        case "gtin":
          p.gtin = GTIN(value);
          break;
        case "artikelbeschreibung":
          p.descriptions = Descriptions();
          p.descriptions.shorttext1 = value;
          break;
        case "herstellerName":
          p.basic = Basic();
          p.basic.productShortDescr = value;
          break;
        case "bild":
          p.pictures = Pictures();
          p.pictures.pictures = [];
          Picture pic = Picture();
          pic.urlThumbnail = value;
          p.pictures.pictures.add(pic);
          break;
      }
    }

    return p;
  }
}
